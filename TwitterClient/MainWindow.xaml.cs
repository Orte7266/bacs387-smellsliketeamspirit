﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Tweetinvi;
using TwitterClient.Classes;
using System.Threading;
using System.Runtime.InteropServices;
using System.Windows.Interop;
using System.Windows.Threading;

namespace TwitterClient
{
    #region enums and structs for blur
    // what follows is a blur/transparency attempt
    internal enum AccentState
    {
        ACCENT_DISABLED = 1,
        ACCENT_ENABLE_GRADIENT = 0,
        ACCENT_ENABLE_TRANSPARENTGRADIENT = 2,
        ACCENT_ENABLE_BLURBEHIND = 3,
        ACCENT_INVALID_STATE = 4
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct AccentPolicy
    {
        public AccentState AccentState;
        public int AccentFlags;
        public int GradientColor;
        public int AnimationId;
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct WindowCompositionAttributeData
    {
        public WindowCompositionAttribute Attribute;
        public IntPtr Data;
        public int SizeOfData;
    }

    internal enum WindowCompositionAttribute
    {
        // ...
        WCA_ACCENT_POLICY = 19
        // ...
    }
    #endregion
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // For blur purposes
        [DllImport("user32.dll")]
        internal static extern int SetWindowCompositionAttribute(IntPtr hwnd, ref WindowCompositionAttributeData data);

        // For debug purposes
        Classes.User CurrentUser;
        LoadingWindow window;

        public MainWindow()
        {
            //Start One Thready Boi For the Loading Animation
            ShowLoadingWindow();
            UpdateStatus(window, "Initializing GUI");
            InitializeComponent();

            //Initial TweetInvi Application Configuration
            UpdateStatus(window, "Configuring Tweetinvi");
            TweetinviConfig.ApplicationSettings.TweetMode = TweetMode.Extended;
            TweetinviConfig.CurrentThreadSettings.TweetMode = TweetMode.Extended;
            RateLimit.RateLimitTrackerMode = RateLimitTrackerMode.TrackOnly;
            SetRateLimitsTracker();

            //Authenticate Default User
            UpdateStatus(window, "Authenticating User");
            CurrentUser = new Classes.User(true);
            UpdateStatus(window, "Fetching Content");

            Startup DoStuff = new Startup(this, CurrentUser);
            DoStuff.FormInit();
            DoStuff.UserContentInit();
            DoStuff.FormContentInit();          

            //Safely Stop Thready Boi Loading Aninmation
            CloseWindowSafe(window);
        }

        #region Methods - Loading Window Access Delegation
        private void ShowLoadingWindow()
        {
            Thread newWindowThread = new Thread(new ThreadStart(ThreadStartingPoint));
            newWindowThread.SetApartmentState(ApartmentState.STA);
            newWindowThread.IsBackground = true;
            newWindowThread.Start();
            Thread.Sleep(250); //Give Time For Loading Window to Initialize
        }
        private void ThreadStartingPoint()
        {
            LoadingWindow tempWindow = new LoadingWindow();
            window = tempWindow;
            tempWindow.Show();
            Dispatcher.Run();
        }

        void CloseWindowSafe(Window w)
        {
            if (w.Dispatcher.CheckAccess())
                w.Close();
            else
                w.Dispatcher.Invoke(DispatcherPriority.Normal, new ThreadStart(w.Close));
        }

        void UpdateStatus(LoadingWindow w, string status)
        {
            try
            {
                if (w.Dispatcher.CheckAccess())
                {
                    w.statusLabel.Content = status;
                }
                else
                    w.Dispatcher.Invoke(DispatcherPriority.Normal, new ThreadStart(() => w.statusLabel.Content = status));
            }
            catch (Exception) { }
        }
        #endregion

        #region Methods - Blur Related       
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            EnableBlur();
        }
        internal void EnableBlur()
        {
            var windowHelper = new WindowInteropHelper(this);

            var accent = new AccentPolicy();
            accent.AccentState = AccentState.ACCENT_ENABLE_BLURBEHIND;

            var accentStructSize = Marshal.SizeOf(accent);

            var accentPtr = Marshal.AllocHGlobal(accentStructSize);
            Marshal.StructureToPtr(accent, accentPtr, false);

            var data = new WindowCompositionAttributeData();
            data.Attribute = WindowCompositionAttribute.WCA_ACCENT_POLICY;
            data.SizeOfData = accentStructSize;
            data.Data = accentPtr;

            SetWindowCompositionAttribute(windowHelper.Handle, ref data);

            Marshal.FreeHGlobal(accentPtr);
        }

        #region Method Handlers - Menu "Buttons"
        private void Window_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            DragMove();
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
        }

        private void Min_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            WindowState = WindowState.Minimized;
        }

        private void Exit_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            this.Close();
        }

        private void Max_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (WindowState == WindowState.Maximized)
            {
                WindowState = WindowState.Normal;
            }
            else
            {
                WindowState = WindowState.Maximized;
            }
        }

        #endregion
        #endregion

        #region Method - Populate the Trending Btns
        public void GetTrendContent()
        {

            String[] names = Trend.RetrieveTopFive();
            btnTrend1.Content = names[0];
            btnTrend2.Content = names[1];
            btnTrend3.Content = names[2];
            btnTrend4.Content = names[3];
            btnTrend5.Content = names[4];
        }
        #endregion

        #region Event Handler - Open Menu Button Clicked
        private void imgOpenMenu_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UtilUI.NavMenuInteract(this);
        }
        #endregion

        #region Event Handlers - Open Menu Button Mouseover (Enter/Exit)
        private void image_MouseEnter(object sender, MouseEventArgs e)
        {
            imgOpenMenu.Source = new BitmapImage(new Uri("pack://application:,,,/Assets/HamburgerMenu-Gray.png"));
        }
        private void image_MouseLeave(object sender, MouseEventArgs e)
        {
            imgOpenMenu.Source = new BitmapImage(new Uri("pack://application:,,,/Assets/HamburgerMenu.png"));
        }
        #endregion

        #region Event Handler - New User Button (On Click)
        private void btnNewAccount_Click(object sender, RoutedEventArgs e)
        {
            ShowLoadingWindow();
            UpdateStatus(window, "Authenticating User");
            CurrentUser = new Classes.User(false);
            UpdateStatus(window, "Fetching Content");
            imgAvatar.Source = CurrentUser.GetAvatar();
            lblName.Content = CurrentUser.GetName();
            lblScreenName.Content = "@" + CurrentUser.GetScreenName();
            CloseWindowSafe(window);
        }
        #endregion

        #region Event Handlers - Sidebar Menu Buttons (On Click)
        private void btnShowSettings_Click(object sender, RoutedEventArgs e)
        {
            using (new WaitCurosr())
            {
                UtilUI.ChangeFocus("pnlSettings",this);
            }
        }

        private void btnShowTimeline_Click(object sender, RoutedEventArgs e)
        {
            using (new WaitCurosr())
            {
                UtilUI.ChangeFocus("pnlTimeline",this);
                scrollTimelineHome.Content = CurrentUser.GetHomeTimelineUI();
            }
        }

        private void btnShowProfile_Click(object sender, RoutedEventArgs e)
        {
            using (new WaitCurosr())
            {
                UtilUI.ChangeFocus("pnlProfile", this);
                scrollTimelineProfile.Content = CurrentUser.GetProfileTimelineUI();
            }
        }

        private void btnShowConversations_Click(object sender, RoutedEventArgs e)
        {
            using (new WaitCurosr())
            {
                UtilUI.ChangeFocus("pnlConversations",this);
                lstConnections.Items.Clear();
                foreach(Tuple<string, long> user in CurrentUser.connections)
                {
                    ComboboxItem item = new ComboboxItem();
                    item.Text = user.Item1;
                    item.Value = user.Item2;
                    lstConnections.Items.Add(item);
                }
                lstConnections.SelectedIndex = 0;
                StackPanel pnlConversations = CurrentUser.GetConversationsUI();
                foreach (Border conversationItem in pnlConversations.Children)
                {
                    conversationItem.Child.MouseLeftButtonDown += (Sender, EventArgs) => OpenConversation(sender, EventArgs, conversationItem.Uid);
                }
                scrollConversations.Content = pnlConversations;
            }
        }
        #endregion

        #region Event Handler - Publish Tweet Button (On Click)
        private void btnPublishTweet_Click(object sender, RoutedEventArgs e)
        {
            CurrentUser.PublishTweet(txtTweet.Text);
            scrollTimelineHome.Content = CurrentUser.GetHomeTimelineUI();
            scrollTimelineProfile.Content = CurrentUser.GetProfileTimelineUI();
            txtTweet.Text = "";
            MessageBox.Show("Tweet Posted", "Success");
        }
        #endregion

        #region Method - Initialize Query Rate Limits Tracker
        private void SetRateLimitsTracker()
        {
            TweetinviEvents.QueryBeforeExecute += (sender, args) =>
            {
                try
                {
                    var queryRateLimits = RateLimit.GetQueryRateLimit(args.QueryURL);

                    // Some methods are not RateLimited. Invoking such a method will result in the queryRateLimits to be null
                    if (queryRateLimits != null)
                    {
                        if (queryRateLimits.Remaining > 0)
                        {
                            // We have enough resource to execute the query
                            Utils.Cout("Query Able to Execute, Remaining: " + queryRateLimits.Remaining);
                            return;
                        }

                        // Strategy #1 : Wait for RateLimits to be available
                        try
                        {
                            UpdateStatus(window, "Awaiting Rate Limits: " + queryRateLimits.ResetDateTime.ToLongTimeString());
                        }
                        catch(Exception) { MessageBox.Show("Awaiting Rate Limits: " + queryRateLimits.ResetDateTime.ToLongTimeString()); }
                        Thread.Sleep((int)queryRateLimits.ResetDateTimeInMilliseconds);
                        //
                        // Strategy #3 : Cancel Query
                        //args.Cancel = true;

                        // Strategy #4 : Implement yours!
                    }
                }

                catch (Exception ex)
                {
                    Utils.Cout("Rate Limit Tracking Failed: " + ex.Message);
                }

            };
        }
        #endregion

        #region Methods - btn handlers for the 5 trend buttons + Refresh. 
        private void btnTrend1_Click(object sender, RoutedEventArgs e)
        {
            UtilUI.GenerateTrendPanel(this,0);
        }

        private void btnTrend2_Click(object sender, RoutedEventArgs e)
        {
            UtilUI.GenerateTrendPanel(this, 1);
        }

        private void btnTrend3_Click(object sender, RoutedEventArgs e)
        {
            UtilUI.GenerateTrendPanel(this, 2);
        }

        private void btnTrend4_Click(object sender, RoutedEventArgs e)
        {
            UtilUI.GenerateTrendPanel(this, 3);
        }

        private void btnTrend5_Click(object sender, RoutedEventArgs e)
        {
            UtilUI.GenerateTrendPanel(this, 4);
        }

        private void btnTrendRefresh_Click(object sender, RoutedEventArgs e)
        {
            GetTrendContent();
        }


        #endregion

        #region Event Handlers - Conversation Click
        public void OpenConversation(object sender, RoutedEventArgs e, string conversationID)
        {
            long Id = Convert.ToInt64(conversationID);
            scrollMessages.Content = CurrentUser.GetMessagesUI(Id);
            btnSendMessage.Uid = Id.ToString();
            UtilUI.ChangeFocus("pnlMessages",this);
        }

        #endregion

        private void Exit_MouseEnter(object sender, MouseEventArgs e)
        {
            exit1.Stroke = Brushes.Red;
            exit2.Stroke = Brushes.Red;
            Cursor = Cursors.Hand;
        }

        private void Exit_MouseLeave(object sender, MouseEventArgs e)
        {
            exit1.Stroke = Brushes.Black;
            exit2.Stroke = Brushes.Black;
            Cursor = Cursors.Arrow;
        }

        private void btnSendMessage_Click(object sender, RoutedEventArgs e)
        {
            if (txtMessages.Text == "") { MessageBox.Show("Please Enter a Message"); return; }
            else CurrentUser.SendMessage(txtMessages.Text, Convert.ToInt64(btnSendMessage.Uid));
            txtMessages.Text = "";
            Thread.Sleep(500); //Wait For Send to Go Through
            long Id = Convert.ToInt64(btnSendMessage.Uid);
            scrollMessages.Content = CurrentUser.GetMessagesUI(Id);
        }

        private void btnSendMessageNew_Click(object sender, RoutedEventArgs e)
        {
            if (txtMessageConv.Text == "") { MessageBox.Show("Please Enter a Message"); return; }
            else if (lstConnections.SelectedItem == null) { MessageBox.Show("Please Select A Recipient"); return; }
            ComboboxItem item = lstConnections.SelectedItem as ComboboxItem;
            CurrentUser.SendMessage(txtMessageConv.Text, Convert.ToInt64(item.Value));
            txtMessageConv.Text = "";
            Thread.Sleep(500); //Wait For Send to Go Through
            btnShowConversations.RaiseEvent(new RoutedEventArgs(Button.ClickEvent));
        }

        private void btnTweetsToFile_Click(object sender, RoutedEventArgs e)
        {
            CurrentUser.BackupTweets();
        }

        private void btnMessagesToFile_Click(object sender, RoutedEventArgs e)
        {
            CurrentUser.BackupDMs();
        }
    }


}

