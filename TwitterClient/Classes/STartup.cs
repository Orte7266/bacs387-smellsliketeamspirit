﻿using System;
using System.Windows;

namespace TwitterClient.Classes
{
    class Startup
    {
        MainWindow w;
        User x;

        public Startup(MainWindow window, User currentUser)
        {
            w = window;
            x = currentUser;
        }

        public void FormInit()
        {
            w.Menu.Visibility = System.Windows.Visibility.Collapsed;
            w.WindowStartupLocation = WindowStartupLocation.CenterScreen;            
        }

        public void UserContentInit()
        {
            w.imgAvatar.Source = x.GetAvatar();
            w.lblName.Content = x.GetName();
            w.lblScreenName.Content = "@" + x.GetScreenName();
        }

        public void FormContentInit()
        {
            GetTrendContent();
            w.scrollTimelineHome.Content = x.GetHomeTimelineUI();
        }

        public void GetTrendContent()
        {
            String[] names = Trend.RetrieveTopFive();
            w.btnTrend1.Content = names[0];
            w.btnTrend2.Content = names[1];
            w.btnTrend3.Content = names[2];
            w.btnTrend4.Content = names[3];
            w.btnTrend5.Content = names[4];
        }        

    }
}
