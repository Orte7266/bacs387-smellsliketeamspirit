﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwitterClient.Classes
{
    abstract class TweetObject
    {
        public string Creator_Name;
        public DateTime Timestamp;
        public string Content;
    }
}
