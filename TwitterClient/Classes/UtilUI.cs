﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using Tweetinvi;

namespace TwitterClient.Classes
{
    class UtilUI
    {
        #region - Methods for Menu Effects and Panel Swapping
        public static void NavMenuInteract(MainWindow w)
        {
            w.Menu.Visibility = w.Menu.Visibility == Visibility.Visible
                                                    ? Visibility.Collapsed
                                                    : Visibility.Visible;
        }

        #region Method - Change Focus of Current Panel Based On Sidebar Menu Click
        public static void ChangeFocus(string panelName, MainWindow w)
        {
            List<string> panels = new List<string> { "pnlTimeline", "pnlProfile", "pnlConversations", "pnlMessages", "pnlSettings", "pnlTrending" };
            foreach (string panel in panels)
            {
                if (panel == panelName) { ((Grid)w.FindName(panel)).Visibility = Visibility.Visible; }
                else { ((Grid)w.FindName(panel)).Visibility = Visibility.Hidden; }
            }
            ((StackPanel)w.FindName("Menu")).Visibility = Visibility.Collapsed;
        }
        #endregion

        public static void GenerateTrendPanel(MainWindow w, int index)
        {
            ChangeFocus("pnlTrending", w);
            w.scrollTrendingTweets.Content = Trend.GetTrendingPanel(Trend.GenerateGUIElements(index));
            w.lblTrendContent.Content = Trends.GetTrendsAt(2391279).Trends[index].Name; 
        }

        #endregion
    }
}
