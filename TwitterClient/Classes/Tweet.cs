﻿using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using Tweetinvi.Models;
using Tweetinvi.Models.Entities;

namespace TwitterClient.Classes
{
    internal class Tweet : TweetObject
    {
        
        private List<IMediaEntity> tweetMedia;
        public bool Retweet;
        public string Creator_ScreenName;
        public BitmapImage Creator_Avatar;

        #region Constructor
        public Tweet(ITweet tweet)
        {
            Retweet = tweet.IsRetweet;

            if (Retweet)
            {
                tweet = tweet.RetweetedTweet;
            }
            Content = tweet.FullText;
            Timestamp = tweet.CreatedAt;
            Creator_Name = tweet.CreatedBy.Name;
            Creator_ScreenName = tweet.CreatedBy.ScreenName;
            Creator_Avatar = Utils.ImageFromURI(tweet.CreatedBy.ProfileImageUrl400x400);
            tweetMedia = tweet.Media;

        }
        #endregion        

        #region Public Method - Get GUI Element for Tweet
        public Border GetGUIElement()
        {
            Border panelBorder = new Border();
            panelBorder.Width = double.NaN;
            panelBorder.Height = 125;
            
            panelBorder.BorderBrush = Brushes.Black;

            DockPanel tweetObject = new DockPanel();
            tweetObject.Width = Double.NaN;
            tweetObject.Height = 125;
            tweetObject.Background = Brushes.Gray;
            tweetObject.Opacity = 1;

            //Avatar of Tweet Creator
            Ellipse tweetImage =  Utils.GetAvatarUI(Creator_Avatar);
            DockPanel.SetDock(tweetImage, Dock.Left);
            tweetObject.Children.Add(tweetImage);

            //Tweet Contents
            Grid tweetContents = new Grid();
            tweetContents.Width = double.NaN;
            DockPanel.SetDock(tweetContents, Dock.Top);

            TextBlock tweetText = new TextBlock();

            //User's Name Header
            Run tweetNameLine = new Run(Creator_Name + Environment.NewLine);
            tweetNameLine.FontWeight = FontWeights.Bold;
            tweetText.Inlines.Add(tweetNameLine);

            //User's Screen Name & Timestamp
            Run tweetScreenAndTimeLine = new Run("@" + Creator_ScreenName + " - " + Timestamp + Environment.NewLine);
            tweetScreenAndTimeLine.FontSize = 10;
            tweetScreenAndTimeLine.Foreground = System.Windows.Media.Brushes.Gray;
            tweetText.Inlines.Add(tweetScreenAndTimeLine);

            //Tweet Contents
            Run tweetTextContents = new Run(Content);
            tweetText.Inlines.Add(tweetTextContents);

            tweetText.Width = double.NaN;
            tweetText.TextWrapping = TextWrapping.Wrap;

            tweetContents.Children.Add(tweetText);

            tweetObject.Children.Add(tweetContents);

            panelBorder.Child = tweetObject;


            // Attempting to add images to the border
            WebClient wc = new WebClient();
            foreach (IMediaEntity media in tweetMedia)
            {
                if (media.MediaType == "photo")
                {
                    //Double Border Height if Image
                    panelBorder.Height = 200;
                    tweetObject.Height = 200;
                    var newImage = Utils.ImageFromURI(media.MediaURLHttps);

                    // Create a rectangle to hold the image
                    Rectangle image = new Rectangle();
                    image.Height = 200;
                    image.Width = 200;
                    image.VerticalAlignment = VerticalAlignment.Bottom;
                    image.HorizontalAlignment = HorizontalAlignment.Left;
                    image.Fill = new ImageBrush(newImage);

                    DockPanel.SetDock(image, Dock.Bottom);
                    tweetObject.Children.Add(image);
                }
            }
            return panelBorder;
        }
        #endregion
    }
}


