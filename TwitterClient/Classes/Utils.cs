﻿using System;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using System.IO;
using System.Windows.Input;
using System.Windows.Shapes;
using System.Windows;
using System.Windows.Media;

namespace TwitterClient.Classes
{
    class Utils
    {
        #region Static Method - Console Output w/ Timestmap
        public static void Cout(string line)
        {
            Console.WriteLine("[" + DateTime.Now + "]: " + line);
        }
        #endregion

        #region Static Method - Bitmap Image from URI
        public static BitmapImage ImageFromURI(string URI)
        {
            try
            {
                BitmapImage bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(URI, UriKind.Absolute);
                bitmap.EndInit();
                return bitmap;
            }
            catch (Exception ex)
            {
                Cout("Fetch and Convert Failed: " + ex.Message);
                return new BitmapImage();
            }
        }
        #endregion

        #region Static Method - Avatar UI Element
        public static Ellipse GetAvatarUI (BitmapImage Avatar)
        {
            Ellipse AvatarUI = new Ellipse();
            AvatarUI.Width = 75;
            AvatarUI.Height = 75;
            AvatarUI.VerticalAlignment = VerticalAlignment.Center;
            AvatarUI.HorizontalAlignment = HorizontalAlignment.Left;
            AvatarUI.Fill = new ImageBrush(Avatar);
            AvatarUI.Margin = new Thickness(10, 0, 10, 0);
            return AvatarUI;
        }
        public static Ellipse GetAvatarUI(long ID)
        {
            BitmapImage Avatar = ImageFromURI(Tweetinvi.User.GetUserFromId(ID).ProfileImageUrl400x400);
            return GetAvatarUI(Avatar);
        }
        #endregion

        #region Static Method - Objects to File 

        public static void ObjectsToFile(List<TweetObject> content)
        {
            using (StreamWriter sw = new StreamWriter("SavedContent.txt", true))
            {
                
                foreach (TweetObject x in content)
                {
                    sw.WriteLine(x.Creator_Name);
                    sw.WriteLine(x.Timestamp);
                    sw.WriteLine(x.Content);
                    sw.WriteLine("");
                }
            }
        }


        #endregion

        
    }

    #region Disposable Class - Wait Cursor
    public class WaitCurosr : IDisposable
    {
        private Cursor previousCursor;

        public WaitCurosr()
        {
            previousCursor = Mouse.OverrideCursor;
            Mouse.OverrideCursor = Cursors.Wait;
        }
        public void Dispose()
        {
            Mouse.OverrideCursor = previousCursor;
        }
    }
    #endregion

    #region Override Class - Combo Box Item
    public class ComboboxItem
    {
        public string Text { get; set; }
        public object Value { get; set; }

        public override string ToString()
        {
            return Text;
        }
    }
    #endregion
}
