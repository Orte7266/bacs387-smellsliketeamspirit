﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using Tweetinvi;
using Tweetinvi.Models;

namespace TwitterClient.Classes
{
    class Trend
    {
        public static String[] RetrieveTopFive()
        {
            String[] temp = new String[5];
            for (int i = 0; i < 5; i++)
            {
                // Retrieves the trends at the Yahoo GEO ID for Denver. 
                temp[i] = Trends.GetTrendsAt(2391279).Trends[i].Name;
            }
            return temp;
        }

        private static List<Tweet> GetTrendingTweets(IEnumerable<ITweet> tweets)
        {
            Utils.Cout("Fetching trending tweets.");
            List<Tweet> trendingTweets = new List<Tweet>();
            if (tweets != null)
            {
                foreach (ITweet x in tweets)
                {
                    Tweet temp = new Tweet(x);
                    trendingTweets.Add(temp);
                }
                return trendingTweets;
            }
            else { Utils.Cout("No trending tweets found."); return null; }
        }

        public static List<UIElement> GenerateGUIElements(int index)
        {
            List<Tweet> Tweets = GetTrendingTweets(Search.SearchTweets(Trends.GetTrendsAt(2391279).Trends[index].Name));
            if (Tweets == null || Tweets.Count == 0) { Utils.Cout("No Trending Tweets Found - Could Not Display Trendline"); return null; }
            Tweet last = Tweets[Tweets.Count - 1];

            List<UIElement> ListOfTweetUI = new List<UIElement>();

            // Return a list of Borders in a uiList. 
            foreach (Tweet x in Tweets)
            {
                Border panelBorder = x.GetGUIElement();
                panelBorder.Padding = new Thickness(15);
                ListOfTweetUI.Add(panelBorder);
            }
            return ListOfTweetUI;
        }

        public static StackPanel GetTrendingPanel(List<UIElement> elements)
        {
            // Returns a stackpanel filled with the tweet border objects. 
            StackPanel trendPanel = new StackPanel();
            trendPanel.Orientation = Orientation.Vertical;
            foreach(UIElement x in elements)
            {
                trendPanel.Children.Add(x);
            }
            return trendPanel;
        }
    }
}
