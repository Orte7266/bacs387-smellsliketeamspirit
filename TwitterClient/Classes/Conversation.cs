﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using Tweetinvi.Models;

namespace TwitterClient.Classes
{
    internal class Conversation
    {
        public long User;
        public IUser userObj;
        public List<Message> Messages;

        #region Constructor
        public Conversation(long user, List<Message> messages)
        {
            User = user;
            Messages = messages;
            userObj = Tweetinvi.User.GetUserFromId(User);
        }
        #endregion

        #region Method - Get Conversation From User
        public static List<Conversation> GetConversations(IAuthenticatedUser user)
        {
            IEnumerable<IMessage> messages = user.GetLatestMessagesReceived(maximumNumberOfMessagesToRetrieve: 40);
            List<Conversation> returnConversations = new List<Conversation>();
            foreach (IMessage message in messages)
            {

                if (returnConversations.FindIndex(f => f.User == message.SenderId) < 0 && message.SenderId != Tweetinvi.User.GetAuthenticatedUser().Id)
                {
                    returnConversations.Add(new Conversation(message.SenderId, new List<Message>()));
                }
            }
            foreach (long userID in returnConversations.Select(f => f.User).Distinct().ToList())
            {
                foreach (IMessage message in messages)
                {
                    if (message.SenderId == userID)
                    {
                        returnConversations.Find(f => f.User == message.SenderId).Messages.Add(new Message(message));
                    }
                }
            }
            messages = user.GetLatestMessagesSent(maximumNumberOfMessagesToRetrieve: 40);
            foreach (IMessage message in messages)
            {
                if (returnConversations.FindIndex(f => f.User == message.RecipientId) < 0 && message.RecipientId != Tweetinvi.User.GetAuthenticatedUser().Id)
                {
                    returnConversations.Add(new Conversation(message.RecipientId, new List<Message>()));
                }
            }
            foreach (long userID in returnConversations.Select(f => f.User).Distinct().ToList())
            {
                Utils.Cout(userID.ToString());
                foreach (IMessage message in messages)
                {
                    if (message.RecipientId == userID)
                    {
                        returnConversations.Find(f => f.User == message.RecipientId).Messages.Add(new Message(message));
                    }
                }
            }
            return returnConversations;
        }
        #endregion

        #region Method - Get Conversation GUI Element
        public Border DisplayGUIElement()
        {
            Border pnlBorder = new Border();
            pnlBorder.Width = double.NaN;
            pnlBorder.Height = 100;
            pnlBorder.Uid = User.ToString();

            pnlBorder.BorderBrush = Brushes.Black;

            DockPanel conversationObject = new DockPanel();
            conversationObject.Width = Double.NaN;
            conversationObject.Height = 100;
            conversationObject.Cursor = Cursors.Hand;
            conversationObject.Background = Brushes.Gray;
            conversationObject.Opacity = 1;

            Ellipse conversationAvi = Utils.GetAvatarUI(User);
            DockPanel.SetDock(conversationAvi, Dock.Left);
            conversationObject.Children.Add(conversationAvi);

            Grid conversationSummary = new Grid();
            conversationSummary.Width = double.NaN;
            DockPanel.SetDock(conversationSummary, Dock.Top);

            TextBlock summaryContents = new TextBlock();
            summaryContents.Width = double.NaN;
            summaryContents.TextWrapping = TextWrapping.Wrap;

            Run conversationUsername = new Run(userObj.Name + Environment.NewLine);
            conversationUsername.FontWeight = FontWeights.Bold;
            summaryContents.Inlines.Add(conversationUsername);

            Run conversationScreenname = new Run("@" + userObj.ScreenName + Environment.NewLine);
            summaryContents.Inlines.Add(conversationScreenname);

            Run lastTimestmap = new Run();
            Run lastMessage = new Run();
            if (Messages.Count > 0)
            {
                Message lstMessage = Messages.OrderByDescending(f => f.Timestamp).First();
                lastMessage.Text = lstMessage.Content + Environment.NewLine;
                if (lstMessage.Reciever != Tweetinvi.User.GetAuthenticatedUser().Name)
                {
                    lastTimestmap.Text = "Sent: " + lstMessage.Timestamp + Environment.NewLine;
                }
                else
                {
                    lastTimestmap.Text = "Recieved: " + lstMessage.Timestamp + Environment.NewLine;
                }
            }
            summaryContents.Inlines.Add(lastTimestmap);
            summaryContents.Inlines.Add(lastMessage);


            conversationSummary.Children.Add(summaryContents);
            conversationObject.Children.Add(conversationSummary);
            pnlBorder.Child = conversationObject;
            return pnlBorder;
        }
        #endregion

        #region Method - Displays the messages in the ui by returning a list of borders

        public List<Border> DisplayMessages()
        {
            List<Border> messagesUI = new List<Border>();

            foreach (Message m in Messages.OrderByDescending(f => f.Timestamp))
            {
                Border pnlBorder = new Border();
                pnlBorder.Width = 500; // double.NaN;
                pnlBorder.Height = 100;

                pnlBorder.BorderBrush = Brushes.Black;

                DockPanel messageObject = new DockPanel();
                messageObject.Width = Double.NaN;
                messageObject.Height = 100;
                messageObject.Background = Brushes.Gray;
                messageObject.Opacity = 1;


                Ellipse messageAvi = Utils.GetAvatarUI(m.Sender_Avatar);

                Grid messageSummary = new Grid();
                messageSummary.Width = double.NaN;
                DockPanel.SetDock(messageSummary, Dock.Top);


                TextBlock messageContents = new TextBlock();
                messageContents.Width = double.NaN;
                messageContents.TextWrapping = TextWrapping.Wrap;


                if (m.Reciever_ID != User)
                {
                    pnlBorder.HorizontalAlignment = HorizontalAlignment.Left;
                    DockPanel.SetDock(messageAvi, Dock.Left);
                    messageContents.TextAlignment = TextAlignment.Left;
                }
                else
                {
                    pnlBorder.HorizontalAlignment = HorizontalAlignment.Right;
                    DockPanel.SetDock(messageAvi, Dock.Right);
                    messageContents.TextAlignment = TextAlignment.Right;
                }

                messageObject.Children.Add(messageAvi);
                Run messageSender = new Run(m.Creator_Name + Environment.NewLine);
                messageSender.FontWeight = FontWeights.Bold;
                messageContents.Inlines.Add(messageSender);

                Run messageTimestamp = new Run(m.Timestamp + Environment.NewLine);
                messageContents.Inlines.Add(messageTimestamp);

                Run messageText = new Run(m.Content + Environment.NewLine);
                messageContents.Inlines.Add(messageText);

                messageSummary.Children.Add(messageContents);
                messageObject.Children.Add(messageSummary);
                pnlBorder.Child = messageObject;
                // my new stuff
                pnlBorder.CornerRadius = new CornerRadius(10);
                pnlBorder.Padding = new Thickness(7);
                pnlBorder.Background = Brushes.Gray;
                pnlBorder.Margin = new Thickness(4);
                messagesUI.Add(pnlBorder);
            }
            return messagesUI;
        }
        #endregion

        #region Method - returns list of tweets
        public List<TweetObject> MessagesToList()
        {
            var temp = Messages.Cast<TweetObject>().ToList();
            return temp;
        }
        #endregion
    }
}