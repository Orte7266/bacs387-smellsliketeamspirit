﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using Tweetinvi.Models;

namespace TwitterClient.Classes
{
    internal class Timeline
    {
        #region Globals
        public List<Tweet> Tweets;
        #endregion

        #region Constructor
        public Timeline(IAuthenticatedUser user, string Type)
        {
            if (user == null) { Utils.Cout("Authentication Failed: Could Not Fetch Timeline"); return; }

            //Get User's Timeline
            switch (Type)
            {
                case "Home":
                    GetTweets(user.GetHomeTimeline());
                    break;
                case "Profile":
                    GetTweets(user.GetUserTimeline());
                    break;
            }
        }
        #endregion

        #region Private Method - Get Tweets 
        private void GetTweets(IEnumerable<ITweet> tweets)
        {
            Utils.Cout("Fetching Tweets");
            Tweets = new List<Tweet>();
            //Cycle Through Tweets in Timeline
            if (tweets != null)
            {
                foreach (ITweet tweet in tweets)
                {
                    Tweet tweetObject = new Tweet(tweet);
                    Tweets.Add(tweetObject);
                }
            }
            else
            {
                Utils.Cout("No Tweets Were Found");
            }
        }
        #endregion

        /*
         * MS
         * In general if you handing a Window down to a business object
         * you probably have a double dependency and that's not a 
         * good thing. Usually you want your dependencies to flow
         * one way... either top to bottom or bottom to top.  
         * But to have the Window depend on the Timeline and the 
         * Timeline depend on the Window creates high coupling 
         * which will cause problems down the road. 
         * 
         * In this case you have a dependency on the Window 
         * through the User which creates even more tight 
         * coupling in your code. 
         */
        #region Public Method - Display Timeline
        public List<UIElement> GetUIElements()
        {
            if (Tweets == null || Tweets.Count == 0) { Utils.Cout("No Tweets Found - Could Not Display Timeline"); return null; }
            Utils.Cout("Generating Timeline GUI Elements");
            Tweet last = Tweets[Tweets.Count - 1];

            List<UIElement> lstTweetUIs = new List<UIElement>();

            foreach (Tweet tweet in Tweets)
            {
                Border panelBorder = tweet.GetGUIElement();
                if (tweet.Equals(last))
                {
                    panelBorder.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                else
                {
                    panelBorder.BorderThickness = new Thickness(2, 2, 2, 0);
                }
                lstTweetUIs.Add(panelBorder);
            }
            return lstTweetUIs;
        }
        #endregion

        #region Method - returns list of tweets
        public List<TweetObject> TweetsToList()
        {
            var temp = Tweets.Cast<TweetObject>().ToList();
            return temp;
        }
        #endregion

    }
}