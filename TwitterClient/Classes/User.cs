﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using Tweetinvi;
using Tweetinvi.Models;

namespace TwitterClient.Classes
{
    class User
    {
        #region Globals
        // TimeLine objects for user/profile pages.
        public Timeline UserTimeline;
        public Timeline ProfileTimeline;
        // List of conversation objects.
        public List<Conversation> CurrentMessages;

        // Credentials, Authentication, and Tweet Invi Objects
        private string CONSUMER_KEY = ConfigurationManager.AppSettings["ConsumerKey"];
        private string CONSUMER_SECRET = ConfigurationManager.AppSettings["ConsumerSecret"];

        private string ACCESS_TOKEN = ConfigurationManager.AppSettings["AccessToken"];
        private string ACCESS_TOKEN_SECRET = ConfigurationManager.AppSettings["AccessSecret"];

        ITwitterCredentials userCredentials;
        IAuthenticatedUser authenticatedUser;

        public List<Tuple<string, long>> connections = new List<Tuple<string, long>>();
        #endregion

        #region Constructor
        // Constructor that takes a boolean to determine if the user is the default stored user. 
        public User(bool defaultUser)
        {
            if (defaultUser) { AuthenticateUser_Default(); }
            else { AuthenticateUser(); }
            GetTimeline();
            GetProfileTimeline();
            GetAllMessages();
            // A friend getting loop that retrives the users "friends" ie. followers/follows for DM purposes. 
            foreach (long ID in authenticatedUser.GetFriendIds())
            {
                if (connections.Where(f => f.Item2 == ID).Count() == 0) { connections.Add(new Tuple<string, long>(Tweetinvi.User.GetUserFromId(ID).ScreenName, ID)); }
            }
            foreach(long ID in authenticatedUser.GetFollowerIds())
            {
                if (connections.Where(f => f.Item2 == ID).Count() == 0) { connections.Add(new Tuple<string, long>(Tweetinvi.User.GetUserFromId(ID).ScreenName, ID)); }

            }
        }
        #endregion

        #region Private Method - Get Authenticated User
        private void AuthenticateUser()
        {
            // Create a new set of credentials for the application.
            var appCredentials = new TwitterCredentials(CONSUMER_KEY, CONSUMER_SECRET);

            // Init the authentication process and store the related `AuthenticationContext`.
            var authenticationContext = AuthFlow.InitAuthentication(appCredentials);

            // Go to the URL so that Twitter authenticates the user and gives him a PIN code.
            Process.Start(authenticationContext.AuthorizationURL);

            // Ask the user to enter the pin code given by Twitter
            // Even in WPF/C# I can't Escape Visual Basic this is the worst.
            string pinCode = Microsoft.VisualBasic.Interaction.InputBox("Enter The Pin", "Pin", "");

            // With this pin code it is now possible to get the credentials back from Twitter
            var userCredentials = AuthFlow.CreateCredentialsFromVerifierCode(pinCode, authenticationContext);

            // Use the user credentials in your application
            Auth.SetCredentials(userCredentials);
            authenticatedUser = Tweetinvi.User.GetAuthenticatedUser(userCredentials);
        }
        private void AuthenticateUser_Default()
        {
            //Authenticate User
            Utils.Cout("Authenticating User");
            userCredentials = Auth.CreateCredentials(CONSUMER_KEY, CONSUMER_SECRET, ACCESS_TOKEN, ACCESS_TOKEN_SECRET);
            authenticatedUser = global::Tweetinvi.User.GetAuthenticatedUser(userCredentials);
            Auth.SetCredentials(userCredentials);

            //Log Authentication Status
            if (authenticatedUser == null) { Utils.Cout("Authentication Failed - Invalid Credentials"); }
            else { Utils.Cout("Authentication Successful"); }
        }
        #endregion

        #region Private Method - Get User Timeline
        private void GetTimeline()
        {
            UserTimeline = new Timeline(authenticatedUser, "Home");
        }
        #endregion

        #region Private Method - Get Profile Timeline
        private void GetProfileTimeline()
        {
            ProfileTimeline = new Timeline(authenticatedUser, "Profile");
        }
        #endregion

        #region Public Method - Get Name
        public string GetName()
        {
            if (authenticatedUser == null) { Utils.Cout("User Not Authenticated - Failed to Fetch Name"); return null; }
            return authenticatedUser.Name;
        }
        #endregion

        #region Public Method - Get Screen Name
        public string GetScreenName()
        {
            if (authenticatedUser == null) { Utils.Cout("User Not Authenticated - Failed to Fetch Screen Name"); return null; }
            return authenticatedUser.ScreenName;
        }
        #endregion

        #region Public Method - Get Avatar
        public BitmapImage GetAvatar()
        {
            Utils.Cout("Fetching Avatar Image and Converting to Bitmap");
            return Utils.ImageFromURI(authenticatedUser.ProfileImageUrl400x400);
        }
        #endregion

        #region Public Method - Display Timeline
        public StackPanel GetHomeTimelineUI()
        {
            GetTimeline();
            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;
            foreach (UIElement tweet in UserTimeline.GetUIElements())
            {
                panel.Children.Add(tweet);
            }
            return panel;
        }
        #endregion

        #region Public Method - Display Profile Timeline
        public StackPanel GetProfileTimelineUI()
        {
            GetProfileTimeline();
            StackPanel panel = new StackPanel();
            panel.Orientation = Orientation.Vertical;
            foreach (UIElement tweet in ProfileTimeline.GetUIElements())
            {
                panel.Children.Add(tweet);
            }
            return panel;
        }
        #endregion

        #region Public Method - Get Messages
        public void GetAllMessages()
        {
            CurrentMessages = Conversation.GetConversations(authenticatedUser);
        }
        #endregion


        #region Methods - Backup methods for user,profile timelines and DM's
        public void BackupDMs()
        {
            foreach(Conversation x in CurrentMessages)
            {
                Utils.ObjectsToFile(x.MessagesToList());
            }
        }

        public void BackupTweets()
        {
            Utils.ObjectsToFile(UserTimeline.TweetsToList());
            Utils.ObjectsToFile(ProfileTimeline.TweetsToList());
        }
        #endregion

        public StackPanel GetConversationsUI()
        {
            GetAllMessages();
            Conversation last = CurrentMessages.Last();
            StackPanel pnlConversations = new StackPanel();
            foreach (Conversation conversation in CurrentMessages)
            {
                Border convGUIElement = conversation.DisplayGUIElement();
                if (conversation.Equals(last))
                {
                    convGUIElement.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                else
                {
                    convGUIElement.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                pnlConversations.Children.Add(convGUIElement);
            }
            return pnlConversations;
        }

        public StackPanel GetMessagesUI(long ID)
        {
            Conversation selectedConversation = CurrentMessages.Find(f => f.User == ID);
            Border lastUI = selectedConversation.DisplayMessages().Last();
            StackPanel pnlMessages = new StackPanel();

            foreach(Border messageUI in selectedConversation.DisplayMessages())
            {
                if (messageUI.Equals(lastUI))
                {
                    messageUI.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                else
                {
                    messageUI.BorderThickness = new Thickness(2, 2, 2, 2);
                }
                pnlMessages.Children.Add(messageUI);
            }
            return pnlMessages;
        }

        public void SendMessage(string message, long ID)
        {
            Tweetinvi.Message.PublishMessage(message, ID);
            Utils.Cout("SendMessage called");
        }

        public void PublishTweet(string tweetContents)
        {
            var tweet = Tweetinvi.Tweet.PublishTweet(tweetContents);
        }
    }
}
