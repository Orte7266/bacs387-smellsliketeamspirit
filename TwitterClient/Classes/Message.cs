﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Media.Imaging;
using Tweetinvi.Models;

namespace TwitterClient.Classes
{
    internal class Message : TweetObject
    {
        public string Reciever;
        public long Reciever_ID;
        public BitmapImage Sender_Avatar;
        public BitmapImage Reciever_Avatar;

        public Message(IMessage message)
        {
            Content = message.Text;
            Timestamp = message.CreatedAt;
            Creator_Name = message.Sender.Name;
            Reciever = message.Recipient.Name;
            Reciever_ID = message.RecipientId;
            Sender_Avatar = Utils.ImageFromURI(message.Sender.ProfileImageUrl400x400);
            Reciever_Avatar = Utils.ImageFromURI(message.Recipient.ProfileImageUrl400x400);
        }

    }
}